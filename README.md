# 기계학습 풀 페이지 만들기

## 목적
   * 영어고자 한국인들을 위한 기계학습 레퍼런스 집합소를 만든다.

## 아키텍쳐
   * 백엔드 : 장고, MySQL
   * 프론트엔드 : 수많은 JS

## Wiki 작성 List
   * React
   * Redux
   * underscore
   * Django
   * Django Rest
   * MaterializeCSS
   * React-bootstrap

